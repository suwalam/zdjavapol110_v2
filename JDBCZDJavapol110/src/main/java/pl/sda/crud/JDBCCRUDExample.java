package pl.sda.crud;

import pl.sda.util.JDBCUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCCRUDExample {

    public static void main(String[] args) {

        String insert = "INSERT INTO person VALUES(4, 'Adam', 'Małysz', '12345678912')";

        try(Connection connection = JDBCUtil.getConnection()) {

            final Statement statement = connection.createStatement();
            final int rowCount = statement.executeUpdate(insert);

            System.out.println("Zaktualizowano " + rowCount + " wierszy");

            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
