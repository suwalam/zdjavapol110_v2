package pl.sda.onetomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;

public class OrderMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

//        Client client = new Client(null, "mnowak", null);
//        Order order1 = new Order(null, BigDecimal.valueOf(100.34), "woda", LocalDateTime.now(), client);
//        Order order2 = new Order(null, BigDecimal.valueOf(14534.74), "gaz", LocalDateTime.now(), client);
//
//        client.setOrders(Arrays.asList(order1, order2));
//
//        session.save(order1);
//        session.save(order2);
//        session.save(client);

        final Client clientFromDB = session.get(Client.class, 1);

        System.out.println();

        clientFromDB.getOrders().forEach(o -> System.out.println(o.getProducts()));

        String hql = "SELECT AVG(totalAmount) FROM Order";
        System.out.println(session.createQuery(hql).getSingleResult());

//
//        session.remove(clientFromDB);

        transaction.commit();
        session.close();
        sessionFactory.close();


    }

}
