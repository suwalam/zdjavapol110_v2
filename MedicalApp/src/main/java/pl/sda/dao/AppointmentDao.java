package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.entity.Appointment;

public class AppointmentDao extends AbstractDao<Appointment> {

    public AppointmentDao(Session session, Class<Appointment> clazz) {
        super(session, clazz);
    }

    public void cancel(Appointment appointment) {
        final Transaction transaction = session.beginTransaction();
        session.delete(appointment);
        transaction.commit();
        session.close();
    }
}
