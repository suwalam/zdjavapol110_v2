package pl.sda.dao;

import org.hibernate.Session;

public abstract class AbstractDao<T> {

    protected Session session;

    protected Class<T> clazz;

    public AbstractDao(Session session, Class<T> clazz) {
        this.session = session;
        this.clazz = clazz;
    }

    public void save(T t) {
        session.save(t);
    }

    public T getById(Integer id) {
        return session.get(clazz, id);
    }

    public void update(T t) {
        session.update(t);
    }

    public void delete(Integer id) {
        T t = getById(id);
        session.delete(t);
    }

}
