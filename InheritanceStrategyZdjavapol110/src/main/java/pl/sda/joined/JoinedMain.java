package pl.sda.joined;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.util.HibernateUtil;

public class JoinedMain {

    public static void main(String[] args) {

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();


        EmployeeV3 officeEmployee = new OfficeEmployeeV3("Jan", "Kowalski", "Excel");
        EmployeeV3 director = new DirectorV3("Michał", "Nowak", "księgowośc");

        session.save(officeEmployee);
        session.save(director);

        final DirectorV3 directorFromDB = session.get(DirectorV3.class, 2);

        System.out.println("director from db: " + directorFromDB);

        transaction.commit();
        session.close();
        HibernateUtil.closeSessionFactory();

    }

}
