package pl.sda.joined;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "director_joined")
@PrimaryKeyJoinColumn(name = "id")
public class DirectorV3 extends EmployeeV3 {

    private String department;

    public DirectorV3(String name, String surname, String department) {
        super(null, name, surname);
        this.department = department;
    }

    @Override
    public String toString() {
        return "DirectorV3{" +
                "department='" + department + '\'' +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
