package pl.sda.joined;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "office_emp_joined")
@PrimaryKeyJoinColumn(name = "id")
public class OfficeEmployeeV3 extends EmployeeV3 {

    private String skills;

    public OfficeEmployeeV3(String name, String surname, String skills) {
        super(null, name, surname);
        this.skills = skills;
    }
}
