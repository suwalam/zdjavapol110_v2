package pl.sda.tableperclass;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.util.HibernateUtil;

public class TablePerClassMain {

    public static void main(String[] args) {

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        Employee officeEmployee = new OfficeEmployee("Jan", "Kowalski", "Excel");

        Employee director = new Director("Michał", "Nowak", "administracja");

        session.save(officeEmployee);
        session.save(director);

        transaction.commit();
        session.close();
        HibernateUtil.closeSessionFactory();

    }

}
