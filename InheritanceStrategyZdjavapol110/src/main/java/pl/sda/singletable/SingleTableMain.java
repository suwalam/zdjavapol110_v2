package pl.sda.singletable;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.util.HibernateUtil;

public class SingleTableMain {

    public static void main(String[] args) {

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        EmployeeV2 officeEmployee = new OfficeEmployeeV2("Jan", "Kowalski", "Java");
        EmployeeV2 director = new DirectorV2("Anna", "Nowak", "IT");

        session.save(officeEmployee);
        session.save(director);

        transaction.commit();
        session.close();
        HibernateUtil.closeSessionFactory();

    }

}
